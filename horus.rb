#Horus system health check script
#Generates html-formatted reports for linux instances that can be sent to hipchat or other monitoring/logging tools.

#System name
s_name = `uname -n`
#IP address
s_ip = `ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d'/'`
#Uptime and load statistics
s_load = `uptime`
#Free memory
s_free = `df -m / | tail -1 | awk '{ print $4 }'`
#Total memory
s_total = `df -m / | tail -1 | awk '{ print $2 }'`
#Top 5 processes in order of memory usage
s_proc = `ps auxk-c | head -6`
#System date
s_time = `date`

#Creates the first half of the report (excluding the s_proc values)
report_html = "████████████████████████████████████████████████████████████████████████████████████████████████<table><tr><td>NAME:</td><td>#{s_name}</td></tr><tr><td>LOCALT:</td><td>#{s_time}</td></tr><tr><td>IP:</td><td>#{s_ip}</td></tr><tr><td>LOAD:</td><td>#{s_load}</td></tr><tr><td>MEMORY:</td><td>#{s_free.chomp}/#{s_total}</td></tr></table>\n"

#Adds a visual horizontal break to the end of the first half of the report
report_html << "════════════════════════════════════════════════════════════════════════════════════════════════"

#Splits the s_proc data into individual lines
s_proc_lines = s_proc.split("\n")

#Array to store chunks of data broken into "cells"
fixedlines = []
#For each line in s_proc_lines
s_proc_lines.each do |l|
    #Split the line on spaces
    cells = l.split(" ")
    #Grab the first 10 "cells," which contain everything except the process name and info
    cells[0..9].each do |w|
      #Add the cells to the fixedlines array
      fixedlines.push(w.strip)
    end
    #Join cells 10...n and join them into a string delimited by spaces, then take the first 66 characters of the new string and add them to the fixedlines array
    fixedlines.push(cells[10..-1].join(" ")[0..65])
end

#Transform the fixedlines array into an array of 11-element arrays
fixedlines = fixedlines.each_slice(11).to_a

#Add a new table to the report
report_html << "<table>"

#Iterate over each array (table row) contained in the fixedlines array
fixedlines.each do |row|
  #Add a table row to the report to contain the data in the fixedlines sub-array (aliased to row)
  report_html << + "<tr>"
  #For each element in the row
  row.each do |c|
    #Add a cell to the table containing the element (aliased to c)
    report_html << "<td>#{c}</td>"
  end
  #Close the row
  report_html << "</tr>"
end

#Close the table
report_html << "</table>"

#Plugins go here
unless ARGV.include?("-X")
  if ARGV.include?("-t")    #Temperature Plugin, requires lb-sensors to run
    report_html << "════════════════════════════════════════════════════════════════════════════════════════════════"
    report_html << `ruby ./horus_temps.rb`
  end
end

#Print the generated report to STDOUT.  This output can be directly captured by the calling process.
puts report_html
