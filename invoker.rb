require 'hipchat'

#Change this to the absolute path to the horus directory
@path = "/this/needs/to/be/changed"

#Get the time that the script was initialized
@time = Time.now

#Loads an opt hash from a properly formatted .config.yml file
@opt_hash = YAML.load(File.read("#{@path}/.config.yml"))

#Sets instance variables based on the values in .config.yml
@autolog_room_name= @opt_hash["autolog_room_name"]
@notification_room_name= @opt_hash["notification_room_name"]
@hipchat_url = @opt_hash["hipchat_url"]
@autolog_room = "https://#{@hipchat_url}/chat/room/#{@autolog_room_id}"
@autolog_key = @opt_hash["autolog_key"]
@notification_key = @opt_hash["notification_key"]
@header_image = @opt_hash["header_image"]

class HorusJob
  def initialize(moniker,opt)
    @moniker = moniker
    opt.each do |key,value|
      instance_variable_set("@#{key}", value)
    end
  end

  def moniker
    @moniker
  end

  def loc
    @loc
  end

  def ssh_com
    @ssh_com
  end

  def script_path
    @script_path
  end

  def plugins
    @plugins
  end

  def show
    puts "#{@moniker}:\n#{@loc}\n#{@ssh_com}\n#{@script_path}\n#{@plugins}"
  end

end

def perform(mode,queue)
  #Header image
  report_html = "#{@header_image}"

  queue.each do |job|
    unless job.ssh_com == "HOST"
      report_html << `#{job.ssh_com} ruby #{job.script_path} #{job.plugins}`
    else
      report_html << `ruby #{job.script_path} #{job.plugins}`
    end
  end

  if mode == "live"
    report(report_html,"Hourly healthcheck report generated, <a href=\"#{@autolog_room}\">click here to view.</a>")
  elsif mode == "smoke"
    puts report_html
  else
    puts "Invalid perform mode \"#{mode}\"passed."
  end

end

def pingtest(queue)
  report_html = ""

  queue.each do |job|
    results = `ping -c 3 #{job.loc}`

    if results.include?("100%")
      report_html << "<table><tr><td>#{job.moniker}</td><td>Did not respond to the pingtest as expected.</td></tr><td>Details:</td><td>#{results}</td></tr></table>"
    end
  end

  if report_html != ""
    #Checks to see if the incident has already been noted.
    #This could be expanded to provide detailed historical logging of incidents, but I don't see a need for that right now.
    if File.exist?("#{@path}/pingtest/active_incident.yml")
      incident = YAML.load(File.read("#{@path}/pingtest/active_incident.yml"))
      #If 5 or fewer checks have occurred, increment the count, and then log normally.
      if incident["try_count"] <=5
        incident["try_count"] +=1
        File.open("#{@path}/pingtest/active_incident.yml", 'w') {|f| f.write incident.to_yaml }
        report(report_html,"One or more hosts may be down, <a href=\"#{@autolog_room}\">click here to view detailed results.</a>")
      #If MORE than 5 checks have ocurred and failed, fall back to only reporting hourly.
      else
        #Checks if the invoker was launched at the top of the hour.
        if @time.min == 0
          report(report_html,"One or more hosts may be down, <a href=\"#{@autolog_room}\">click here to view detailed results.</a>")
          incident["try_count"] +=1
          File.open("#{@path}/pingtest/active_incident.yml", 'w') {|f| f.write incident.to_yaml }
        #If not, simply increments the try_count and moves along
        else
          incident["try_count"] +=1
          File.open("#{@path}/pingtest/active_incident.yml", 'w') {|f| f.write incident.to_yaml }
        end
      end

    #If the incident has not been noted, creates an incident for tracking before notifying hipchat.
    else
      incident = {"try_count" => 1}
      File.open("#{@path}/pingtest/active_incident.yml", 'w') {|f| f.write incident.to_yaml }
      report(report_html,"One or more hosts may be down, <a href=\"#{@autolog_room}\">click here to view detailed results.</a>")
    end
  #If the report is empty (indicating that all hosts are up), and an active_incident.yml file exists, delete the file.
  else
    if File.exist?("#{@path}/pingtest/active_incident.yml")
      File.delete("#{@path}/pingtest/active_incident.yml")
    end
  end

end

def report(payload,notification)
  #Send full report to Hipchat (Autolog room)
  client = HipChat::Client.new("#{@autolog_key}",:api_version => 'v2', :server_url => "#{@hipchat_url}")
  client["#{@autolog_room_name}"].send('Horus', "#{payload}", :message_format => 'html', :color => 'purple', :notify =>true )

  #Send report notification to Hipchat (main room)
  client = HipChat::Client.new("#{@notification_key}",:api_version => 'v2', :server_url => "#{@hipchat_url}")
  client["#{@notification_room_name}"].send('Horus', "#{notification}", :message_format => 'html', :color => 'purple' )
end

def main
  queue = []
  @opt_hash["host_list"].each do |key,value|
    queue.push(HorusJob.new(key,value))
  end

  #smoke
=begin
  puts queue
  queue.each do |x|
    puts x.show
  end
=end

  if ARGV[0] == 'l'
    perform("live",queue)
  elsif ARGV[0] == 'p'
    pingtest(queue)
  elsif ARGV[0] == 's'
    perform("smoke",queue)
  else
    puts "Please provide a modal argument.\n
    Available arguments:\n
    l: Runs a full healthcheck of configured nodes and reports to hipchat.\n
    p: Runs a pingtest on configured nodes and reports to hipchat if one or more nodes are unreachable.\n
    s: Runs a smoke test of the invoker system."
  end

end

main
