#Horus system health check temperature logging plugin
#Generates a temperature report
#
#Requires lm-sensors.
#>sudo apt-get install lm-sensors

#Create an empty string to hold the report html
report_html = ""

#System temperatures
s_temps = `sensors`

#Split s_temps into lines
s_temps_lines = s_temps.split("\n")

#Add a new table to the report
report_html << "TEMPERATURES<table>"

#Put the temperature lines into a table
s_temps_lines.each do |l|
    report_html << "<tr>"
    report_html << "<td>#{l}</td>"
    report_html << "</tr>"
end

report_html << "</table>"

#Print the generated report to STDOUT.  This output can be directly captured by the calling process.
puts report_html
