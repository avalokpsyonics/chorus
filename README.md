## Synopsis

Horus is a system healthcheck tool that is built to allow a central server (the invoker) to marshall and create html-formatted reports for use in hipchat from a series of remote client servers.

The system running the invoker script uses ssh to execute the horus.rb script remotely and concatenates the output from STDOUT into a single HTML report, which can then be sent directly to hipchat, or whatever other logging tool you so desire.

## Hipchat Preparation

1. Create a new room for automatic logs.  I generally recommend the name "Autolog."  

2. Have a main room used for chat that can also be used to send notifications that Horus has run.

3. Generate API client keys for the Autolog room and the room where you would like notifications to appear.

## Client Installation

1. Create a `utils` directory in the home-directory of the account that you would like Horus to run under.

2. Run `cd ~/utils` and `git clone` the repository.

3. You're all set.

## Invoker Installation

1. Create a `utils` directory in the home-directory of the account that you would like Horus to run under.

2. Run `cd ~/utils` and `git clone` the repository.

3. Run `cd ~/horus` and `bundle install`.

3. Change line 4 in invoker.rb to match the location of the Horus directory.  This allows cron to reference files in the Horus directory at runtime.

```
Example:
#invoker.rb
4 @path = "/home/youraccount/utils/horus"
```

4. Edit the .config.yml file per the included instructions.

5. Run `crontab -e` and add a job to call `~/utils/horus/invoker.rb [modal options]` at whatever time you frequency you would like.

```
Available Modal Options:

l: Runs a full healthcheck of configured nodes and reports to hipchat.
p: Runs a pingtest on configured nodes and reports to hipchat if one or more nodes are unreachable.
s: Runs a smoke test of the invoker system.
```

## Plugins
### horus_temps
Currently there is only one plugin, `horus_temps`, which adds temperature data to a given client report.  It can be invoked by adding the `-t` flag to the plugins key for a given server in .config.yml.

```
Example
#.config.yml
host_list:
   Target_1:
     loc: 177.177.77.77
     ssh_com: ssh youraccount@177.177.77.77
     script_path: utils/horus/horus.rb
     plugins: -t
```

Enabling `horus_temps` requires that lb-sensors is installed on the client.

`sudo apt-get install lb-sensors`

##License
```
Horus automated monitoring and reporting system.  
Copyright (C) 2017 Dave Nearing III

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
